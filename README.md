# Entrega practica

## Datos
* Nombre: Gonzalo López López
* Titulacion: INGENIERÍA EN SISTEMAS DE TELECOMUNICACIÓN + ADMINISTRACIÓN Y DIRECCIÓN DE EMPRESAS
* Despliegue (url): [glolo.pythonanywhere.com](https://glolo.pythonanywhere.com)
* Video básico (url): [https://www.youtube.com/watch?v=iSnlrB_QSXI](https://www.youtube.com/watch?v=iSnlrB_QSXI)
* Video parte opcional (url): [https://www.youtube.com/watch?v=l6BY6duWW8c](https://www.youtube.com/watch?v=l6BY6duWW8c)
## Cuenta Admin Site
* usuario/contraseña: glolo/glolo

## Cuentas usuarios
* usuario/contraseña: glolo/glolo
* usuario/contraseña: gonzalo/sarourjc

## Resumen parte obligatoria
En la parte obligaroria puede verse una pagina de inicio con las 10 ultimas aportaciones realizadas por los usuarios. Una vez autenticados pueden verse también las 5 últimas aportaciones realizas por el usuario que estemos usando. También puede cambiarse a modo oscuro con el boton que aparece en la parte superior derecha. Las aportaciones extraen todo lo solicitado para Aemet, Reddit, Youtube, Wikipedia y los recursos no reconocidos. Es posible votar aportaciones si estamos loggeados y también añadir nuevas aportaciones. Haciendo uso del menu de la izquierda pueden accederse al resto de páginas del sitio.

Pasando a la página de cada aportación, puede verse estas en formato extendido y realizar comentarios.


En la página del usuario pueden verse todas las aportaciones que ha realizado en formato resumido, todos los comentarios que ha realizado y todos los votos que ha realizado, indicando claramente en que aportaciones ha comentado y votado. 

Por último, se cuenta también con una página de información donde se comenta el objetivo del sitio. 


## Lista partes opcionales
* Nombre parte: Inclusión de un favicon del sitio
* Nombre parte: Visualización de cualquier página en formato JSON y/o XML, de forma similar a como se ha indicado para la página principal.
* Nombre parte: Generación de un documento XML y/o JSON para los comentarios puestos
en el sitio.
* Nombre parte: Inclusión de Reddit como 4º recurso reconocido.
