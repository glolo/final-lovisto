from django.shortcuts import render, redirect, reverse

from django.http import JsonResponse
from django.http import HttpResponse
from django.views import View
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages

from contribution.models import Contribution


# Create your views here.
class BaseView(View):
    context = {}
    template_name = 'base/base.html'
    xml_template_name = 'base/xml/base.xml'
    redirect = False
    dir_redirect = ''
    type_response = ''

    def dispatch(self, request, *args, **kwargs):
        self.context = {}
        if 'format' in request.GET:
            format = request.GET['format']
            if format == 'xml':
                self.type_response = 'xml'
            if format == 'json':
                self.type_response = 'json'


        contributions = Contribution.objects.filter(author__id=request.user.id).exclude(source=None)[::-1][0:3]
        last_access = []
        for contribution in contributions:
            last_access.append(contribution.source)
        
        
        self.context['last_access'] = last_access

        if request.user.is_authenticated and request.user.configuration.dark_mode:
            self.context['dark_mode'] = True

        # raise Exception(self.context)
        return super(BaseView, self).dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        self.base_get(request, *args, **kwargs)
        if request.user.is_authenticated:
            self.auth_get(request, *args, **kwargs)

        # first we redirect
        if self.redirect:
            return redirect(self.dir_redirect)

        # raise Exception(self.context)
        # Before check if it's a special render
        if self.type_response == 'xml':
            return render(request, self.xml_template_name, self.context, content_type='text/xml')
        elif self.type_response == 'json':
            return JsonResponse(self.context)

        # Here we render a single html
        return render(request, self.template_name, self.context)

    def post(self, request, *args, **kwargs):
        self.base_post(request, *args, **kwargs)

        if request.user.is_authenticated:
            self.auth_post(request, *args, **kwargs)

        # first we redirect
        if self.redirect:
            return redirect(self.dir_redirect)

        # Before check if it's a special render
        if self.type_response == 'xml':
            return render(request, self.xml_template_name, self.context)
        elif self.type_response == 'json':
            return JsonResponse(self.context)

        # Here we render a single html
        return render(request, self.template_name, self.context)

    def auth_get(self, request, *args, **kwargs):
        pass

    def base_get(self, request, *args, **kwargs):
        pass

    def auth_post(self, request, *args, **kwargs):
        pass

    def base_post(self, request, *args, **kwargs):
        pass

class LoginView(View):
    def post(self, request, *args, **kwargs):
    
        user = authenticate(request, username=request.POST['user'], password=request.POST['pass'])
        # Correct users and password
        if user is None:
            messages.error(request, 'Usuario erroneo y password no coinciden.')
        else:
            login(request, user)

        return redirect(request.POST['redirect'])

class LogoutView(View):
    def get(self, request, *args, **kwargs):
        logout(request)
        return redirect(reverse('contributions:home'))
