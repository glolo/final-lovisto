from django.db import models

from django.conf import settings


class UserConf(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='configuration')
    dark_mode = models.BooleanField(null=False)
