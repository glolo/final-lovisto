
from django.conf.urls import include, url
from django.contrib import admin
from base import views

app_name = "base"

urlpatterns = [
    url('login', views.LoginView.as_view(), name='login'),
    url('logout', views.LogoutView.as_view(), name='logout'),
    url('', views.BaseView.as_view(), name='base'),
]
