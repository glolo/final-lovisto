import datetime

from django.db import models
from django.conf import settings
from contribution.handle_resources import HandleResources


class Contribution(models.Model):
    name = models.CharField(max_length=50)
    url = models.CharField(max_length=500)
    html = models.TextField()
    description = models.TextField()
    date = models.DateTimeField()
    source = models.CharField(max_length=150, null=True)
    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='contributions')

    def get_contribution_formated(self, user_id):
        all_comments = self.comments.all().order_by('-date')
        all_comments_serializer = []
        for comment in all_comments:
            all_comments_serializer.append(comment.to_json())

        vote_user = None
        if self.votes.filter(user__id=user_id).first():
            vote_user = self.votes.filter(user__id=user_id).first().to_json()

        return {
            'id': self.id,
            'name': self.name,
            'url': self.url,
            'html': self.html,
            'description': self.description,
            'date': self.date,
            'author_id': self.author.id,
            'author_username': self.author.username,
            'like': self.votes.filter(value_vote=True).count(),
            'unlike': self.votes.filter(value_vote=False).count(),
            'user_voted': vote_user,
            'num_comments': len(all_comments_serializer)
        }

    def get_contribution_formated_comments(self, user_id):
        all_comments = self.comments.all().order_by('-date')
        all_comments_serializer = []
        for comment in all_comments:
            all_comments_serializer.append(comment.to_json())


        vote_user = None
        if self.votes.filter(user__id=user_id).first():
            vote_user = self.votes.filter(user__id=user_id).first().to_json()

        return {
            'id': self.id,
            'name': self.name,
            'url': self.url,
            'html': self.html,
            'description': self.description,
            'date': self.date,
            'author_id': self.author.id,
            'author_username': self.author.username,
            'like': self.votes.filter(value_vote=True).count(),
            'unlike': self.votes.filter(value_vote=False).count(),
            'user_voted': vote_user,
            'comments': all_comments_serializer,
            'num_comments': len(all_comments_serializer)
        }

    def get_contribution_formated_summary(self):
        return {
            'id': self.id,
            'name': self.name,
            'url': self.url
        }

    def to_json(self):
        return {
            'name': self.name,
            'url': self.url,
            'html': self.html,
            'description': self.description,
            'date': self.date,
            'author': {'username': self.author.username, 'id': self.author.id},
        }

    def save_contribution_with_resource(self, author):
        handle_resources = HandleResources()

        self.date = datetime.datetime.now()
        self.author = author
        result_handles = handle_resources.identify_resource(self.url)
        self.html = result_handles['html']
        self.source = result_handles['resource']
        self.save()


class Comment(models.Model):
    text = models.TextField()
    date = models.DateTimeField()
    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='comments')
    contribution = models.ForeignKey('Contribution', on_delete=models.CASCADE, related_name='comments')

    def get_comments_formated(self):
        return {
            'id': self.id,
            'text': self.text,
            'date': self.date,
            'id_contribution': self.contribution.id,
            'name_contribution': self.contribution.name,
            'url_contribution': self.contribution.url,
            # 'tiny_url_contribution': self.contribution.url[0:40]+'...'
        }

    def to_json(self):
        return {
            'text': self.text,
            'date': self.date.strftime("%Y-%m-%d %H:%M:%S"),
            'author': {'username': self.author.username, 'id': self.author.id},
            'contribution': self.contribution.to_json()
        }

    def save_comment(self, contribution, author):
        self.date = datetime.datetime.now()
        self.contribution = contribution
        self.author = author
        self.save()


class Vote(models.Model):
    contribution = models.ForeignKey(Contribution, on_delete=models.CASCADE, related_name='votes')
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='votes')
    value_vote = models.BooleanField()

    def get_vote_formated(self):
        return {
            'id': self.id,
            'value_vote': self.value_vote,
            'id_contribution': self.contribution.id,
            'name_contribution': self.contribution.name,
            'url_contribution': self.contribution.url,
            # 'tiny_url_contribution': self.contribution.url[0:40]+'...'
        }

    def to_json(self):
        return {
            'value_vote': self.value_vote,
            'user': {'username': self.user.username, 'id': self.user.id},
            'contribution': self.contribution.to_json()
        }



