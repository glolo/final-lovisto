
from django.conf.urls import include, url
from contribution import views

app_name = "contribution"

urlpatterns = [
    url(r'vote/(?P<id_contribution>[0-9]+)/(?P<value>[0-1])$', views.VoteContributionView.as_view(), name='vote_contribution'),
    url(r'change/mode/(?P<value>[0-1])$', views.ChangeModeView.as_view(), name='change-mode'),
    url(r'user/page/(?P<id_user>[0-9]+)$', views.UserPageView.as_view(), name='user_page'),
    url(r'take/comments/(?P<id_contribution>[0-9]+)$', views.DownloadCommentsView.as_view(), name='download_comments'),
    url(r'(?P<id_contribution>[0-9]+)$', views.SingleContributionView.as_view(), name='single_contribution'),
    url(r'all$', views.AllContributionsView.as_view(), name='all_contributions'),
    url(r'information$', views.InformationPageView.as_view(), name='information_page'),
    url(r'^$', views.HomeView.as_view(), name='home'),
]
