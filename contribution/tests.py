from django.test import TestCase
from contribution.handle_resources import HandleResources
import unittest


class ResourcesTestCase(TestCase):
    handle = None

    def setUp(self):
        self.handle = HandleResources()

    def test_aemet(self):
        html = self.handle.extract_aemet('http://www.aemet.es/es/eltiempo/prediccion/municipios/getafe-id28065')
        self.assertIn('class="aemet"', html)

    def tests_wikipedia(self):
        html = self.handle.extract_wikipedia('https://es.wikipedia.org/wiki/Astronauta')
        self.assertIn('class="wikipedia"', html)

    def tests_youtube(self):
        html = self.handle.extract_youtube('https://www.youtube.com/watch?v=IfoSqaxJsAM&ab_channel=CursosWeb')
        self.assertIn('class="youtube"', html)

    def tests_reddit(self):
        html = self.handle.extract_redit('https://www.reddit.com/r/django/comments/n842st/is_there_a_public_repo_that_shows_productionlevel/')
        self.assertIn('class="reddit"', html)



