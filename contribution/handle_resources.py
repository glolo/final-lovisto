import re
import requests
import xmltodict
import json

from bs4 import BeautifulSoup
from django.template.loader import render_to_string


class HandleResources():

    def make_request(self, url):
        # Reddit need user agent or give an error 429
        response = requests.get(url, headers={'User-agent': 'your bot 0.1'})

        if str(response.status_code) == '200':
            return response
        else:
            raise Exception('Imposible conectar')

    def identify_resource(self, url):

        patterns = [
            {
                'pattern': ".*aemet\.es\/es\/eltiempo\/prediccion\/municipios\/(\w+)-id(\d+)",
                'function': self.extract_aemet
            }, {
               'pattern': ".*\.wikipedia\.org\/wiki\/.*",
               'function': self.extract_wikipedia
           }, {
               'pattern': ".*\.youtube\.com\/watch\?v=.*",
               'function': self.extract_youtube
           }, {
               'pattern': ".*\.reddit\.com\/r\/(\w+)\/comments\/(\w+)\/(\w+)\/",
               'function': self.extract_redit
           }
        ]

        index_pattern = 0
        finish = False
        while not finish and index_pattern < len(patterns):
            result = re.match(patterns[index_pattern]['pattern'], url)
            if result:
                finish = True
            else:
                index_pattern += 1

        if finish:
            result = patterns[index_pattern]['function'](url)
        else:
            result = self.launch_no_resource(url)

        return result

    def extract_aemet(self, url):
        url_for_xml_request = "https://www.aemet.es/xml/municipios/localidad_%s.xml"

        m = re.search('id(\d+)?', url)

        aemet = self.make_request(url_for_xml_request % format(m.group(1)))
        data = xmltodict.parse(aemet.text)
     
        aemet_html_context = {
            'town': data['root']['nombre'],
            'place': data['root']['provincia'],
            'url': url,
            'days': []
        }
        for day in data['root']['prediccion']['dia']:
            day_dict = {
                'date': day['@fecha'],
                'temp_max': day['temperatura']['maxima'],
                'temp_min': day['temperatura']['minima'],
                'sen_term_max': day['sens_termica']['maxima'],
                'sen_term_min': day['sens_termica']['minima'],
                'hum_rel_max': day['humedad_relativa']['maxima'],
                'hum_rel_min': day['humedad_relativa']['minima']
            }

            aemet_html_context['days'].append(day_dict)

        html_res = render_to_string('handle/aemet.html', aemet_html_context)
        return {'html': html_res, 'resource': 'aemet'}

    def extract_wikipedia(self, url):
        wikipedia_context = {}
        m = re.search('.*\.wikipedia\.org\/wiki\/(\w+)?', url)

        text_url = 'https://es.wikipedia.org/w/api.php?action=query&format=xml&titles=%s&prop=extracts&exintro&explaintext'
        image_url = 'https://es.wikipedia.org/w/api.php?action=query&titles=%s&prop=pageimages&format=json&pithumbsize=100'

        wikipedia_text = self.make_request(text_url % format(m.group(1)))
        wikipedia_image = self.make_request(image_url % format(m.group(1)))

        data = xmltodict.parse(wikipedia_text.text)
        wikipedia_json = json.loads(wikipedia_image.text)
        
        values = wikipedia_json['query']['pages'].values()
        iter_object = iter(values)
        wikipedia_context['image'] = next(iter_object)
        wikipedia_context['url'] = url
        wikipedia_context['principal_text'] = data['api']['query']['pages']['page']['extract']['#text'][0:399]
        html_res = render_to_string('handle/wikipedia.html', wikipedia_context)
        return {'html': html_res, 'resource': 'wikipedia'}


    def extract_youtube(self, url):
        url_dest = 'https://www.youtube.com/oembed?format=json&url=%s' % url
        youtube = self.make_request(url_dest)

        youtube_context = json.loads(youtube.text)
        youtube_context['original_url'] = url
        html_res = render_to_string('handle/youtube.html', youtube_context)
        return {'html': html_res, 'resource': 'youtube'}

    def extract_redit(self, url):
        final_url = 'https://www.reddit.com/r/%s/comments/%s/.json'
        m = re.search('r\/(\w+)?\/comments\/(\w+)?', url)

        reddit_data = self.make_request(final_url % (m.group(1), m.group(2)))
        reddit_context = json.loads(reddit_data.text)

        html_res = render_to_string('handle/reddit.html', reddit_context[0]['data']['children'][0]['data'])

        return {'html': html_res, 'resource': 'reddit'}

    def launch_no_resource(self, url):
        no_resource_context = {}
        no_resource_data = self.make_request(url)
        html_obtained = no_resource_data.text
        soup = BeautifulSoup(html_obtained)

        if soup.find("meta",  property="og:title"):
            no_resource_context['have_og'] = True
            if(soup.find("meta",  property="og:title")):
                no_resource_context['title'] = soup.find("meta",  property="og:title")['content']
            else:
                no_resource_context['title'] = " "
            if(soup.find("meta",  property="og:image")):
                no_resource_context['image'] = soup.find("meta",  property="og:image")['content']
            else:
                no_resource_context['image'] = " "
        elif soup.title.string:
            no_resource_context['have_title'] = True
            no_resource_context['title'] = soup.title.string
        else:
            no_resource_context['have_title'] = False

        html_res = render_to_string('handle/resource_not_found.html', no_resource_context)

        return {'html': html_res, 'resource': None}
