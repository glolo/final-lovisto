from django.shortcuts import render, reverse
from base.views import BaseView

from contribution.models import Contribution, Comment, Vote
from contribution.forms import ContributionForm, CommentForm


class HomeView(BaseView):
    template_name = 'contribution/home.html'
    xml_template_name = 'contribution/xml/home.xml'

    def auth_get(self, request, *args, **kwargs):
        self.context['vote_user_id'] = request.user.id
        if not self.type_response:
            self.context['new_contribution_form'] = ContributionForm()

        contributions = Contribution.objects.filter(author__id=request.user.id)[::-1][0:5]
        final_contributions = []
        for contribution in contributions:
            final_contributions.append(contribution.get_contribution_formated_summary())
        self.context['last_user_contributions'] = final_contributions

    def base_get(self, request, *args, **kwargs):

        contributions = Contribution.objects.all()[::-1][0:10]
        final_contributions = []
        for contribution in contributions:
            final_contributions.append(contribution.get_contribution_formated(request.user.id))

        self.context['contributions'] = final_contributions

    def auth_post(self, request, *args, **kwargs):
        if not self.type_response:
            form = ContributionForm(request.POST or None, request.FILES or None)

            # check if form data is valid
            if form.is_valid():

                form.instance.save_contribution_with_resource(request.user)

    def base_post(self, request, *args, **kwargs):
        self.redirect = True
        self.dir_redirect = reverse('contributions:home')


class VoteContributionView(BaseView):
    def auth_get(self, request, *args, **kwargs):
        contribution = Contribution.objects.filter(id=kwargs['id_contribution']).first()
        if contribution:
            vote = Vote.objects.filter(contribution__id=kwargs['id_contribution'], user__id=request.user.id).first()
            value_vote = True if str(kwargs['value']) == str(1) else False
            if vote:
                if vote.value_vote != value_vote:
                    vote.value_vote = value_vote
                    vote.save()
            else:
                vote = Vote()
                vote.contribution = contribution
                vote.user = request.user
                vote.value_vote = value_vote
                vote.save()

    def base_get(self, request, *args, **kwargs):
        self.redirect = True
        self.dir_redirect = request.GET['redirect']


class ChangeModeView(BaseView):

    def auth_get(self, request, *args, **kwargs):

        value_mode = True if str(kwargs['value']) == str(1) else False
        request.user.configuration.dark_mode = value_mode
        request.user.configuration.save()

    def base_get(self, request, *args, **kwargs):
        self.redirect = True
        self.dir_redirect = reverse('contributions:home')


class SingleContributionView(BaseView):
    template_name = 'contribution/single_contribution.html'
    xml_template_name = 'contribution/xml/single_contribution.xml'

    def auth_get(self, request, *args, **kwargs):
        self.context['vote_user_id'] = request.user.id
        if not self.type_response:
            self.context['new_comment_form'] = CommentForm()

    def base_get(self, request, *args, **kwargs):
        contribution = Contribution.objects.filter(id=kwargs['id_contribution']).first()
        if contribution:
            self.context['contribution'] = contribution.get_contribution_formated_comments(request.user.id)
        else:
            self.redirect = True
            self.dir_redirect = reverse('contributions:home')

    def auth_post(self, request, *args, **kwargs):
        form = CommentForm(request.POST or None, request.FILES or None)

        if form.is_valid():
            form.instance.save_comment(self.context['contribution'], request.user)

    def base_post(self, request, *args, **kwargs):
        contribution = Contribution.objects.filter(id=kwargs['id_contribution']).first()
        self.redirect = True

        if contribution:
            self.context['contribution'] = contribution
            self.dir_redirect = reverse('contributions:single_contribution', kwargs={'id_contribution': contribution.id})
        else:
            self.dir_redirect = reverse('contributions:home')


class UserPageView(BaseView):
    template_name = 'contribution/user_page.html'
    xml_template_name = 'contribution/xml/user_page.xml'

    def auth_get(self, request, *args, **kwargs):
        # Taking votes with format
        votes = Vote.objects.filter(user__id=request.user.id)
        final_votes = []
        for vote in votes:
            final_votes.append(vote.get_vote_formated())
        # Taking all comment of user
        comments = Comment.objects.filter(author__id=request.user.id)
        final_comments = []
        for comment in comments:
            final_comments.append(comment.get_comments_formated())

        # Taking all contribution of user
        contributions = Contribution.objects.filter(author__id=request.user.id)
        final_contributions = []
        for contribution in contributions:
            final_contributions.append(contribution.get_contribution_formated_summary())

        self.context['votes'] = final_votes[::-1]
        self.context['comments'] = final_comments[::-1]
        self.context['contributions'] = final_contributions[::-1]

    def base_get(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            self.redirect = True
            self.dir_redirect = reverse('contributions:home')


class AllContributionsView(BaseView):
    template_name = 'contribution/all_contrbutions.html'
    xml_template_name = 'contribution/xml/all_contrbutions.xml'

    def base_get(self, request, *args, **kwargs):
        contributions = Contribution.objects.all().order_by('-date')

        final_contributions = []
        for contribution in contributions:
            final_contributions.append(contribution.get_contribution_formated_summary())
        self.context['contributions'] = final_contributions


class InformationPageView(BaseView):
    template_name = 'contribution/information_page.html'
    xml_template_name = 'contribution/xml/information_page.xml'

    def base_get(self, request, *args, **kwargs):
        self.context['author'] = 'Desarrollado por Gonzalo López López'
        self.context['object'] = 'Este proyecto tiene como objetivo crear una comunidad de personas que compartan y comenten contenidos.'
        self.context['fuctionality'] = "Tenemos un listado de aportaciones hechas por la comunidad, esas aportaciones podrás votarlas y comentarlas, además de añadir nuevas aportaciones propias y hacerles un seguirmiento."

class DownloadCommentsView(BaseView):
    xml_template_name = 'contribution/xml/comments_single_contribution.xml'

    def base_get(self, request, *args, **kwargs):
        if not self.type_response:
            self.type_response = 'xml'

        contribution = Contribution.objects.filter(id=kwargs['id_contribution']).first()
        if contribution:
            self.context['comments'] = contribution.get_contribution_formated_comments(request.user.id)['comments']
        else:
            self.redirect = True
            self.dir_redirect = reverse('contributions:home')

