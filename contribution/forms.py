from django.forms import ModelForm
from contribution.models import Contribution, Comment


class ContributionForm(ModelForm):
    class Meta:
        model = Contribution
        fields = ['name', 'url','description']


class CommentForm(ModelForm):
    class Meta:
        model = Comment
        fields = ['text']
