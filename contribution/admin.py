from django.contrib import admin
from .models import Contribution, Comment, Vote

admin.site.register(Contribution)
admin.site.register(Comment)
admin.site.register(Vote)
